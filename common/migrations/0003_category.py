# Generated by Django 4.2.3 on 2023-07-11 12:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("common", "0002_alter_choice_question_alter_choice_votes"),
    ]

    operations = [
        migrations.CreateModel(
            name="Category",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("title", models.CharField(max_length=30)),
            ],
        ),
    ]
